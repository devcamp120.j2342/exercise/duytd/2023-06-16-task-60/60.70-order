package com.devcamp.task6070.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
    
}
